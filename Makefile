build: passwd.json
	[ -d ./out ] || mkdir ./out
	jq -r '.[] | "<tr><th>\(.manufacturer)</th><td>\(.model)</td><td>\(.protocol // "-")</td><td>\(.login // "n/a")</td><td>\(.passwd // "n/a")</td><td>\(.gateway // "-")</td><td><a href=\"manuals/\(.manual)\">\(.manual // "")</a></td></tr>"' passwd.json > ./out/body.html
	echo '<html><head><title>Manufacturer Default Passwords</title></head><body>' > ./out/index.html
	echo '<p>Best search engine ever: <strong>CTRL+F</strong></p>' >> ./out/index.html
	echo '<p>Dont scrap it, <a href="https://gitlab.com/gghez/0passwd/-/raw/master/passwd.json">JSON raw format is here</a>.</p>' >> ./out/index.html
	echo '<table border="1"><thead><tr><th>Manufacturer</th><th>Model</th><th>Protocol</th><th>Login</th><th>Password</th><th>Gateway</th><th>Manual</th></tr></thead><tbody>' >> ./out/index.html
	cat ./out/body.html >> ./out/index.html
	echo '</tbody></table><p>F**k CSS</p></body></html>' >> ./out/index.html

publish: ./out/index.html
	[ -d ./public ] || mkdir ./public
	cp ./out/index.html ./public/
	cp -r ./manuals ./public/
	cp -r ./thumbnails ./public/

clean:
	rm -r ./{out,public}
